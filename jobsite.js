'use strict';


function __init__() {
  window.setInterval(getDayCount (), 1000);
  colorTransitions();
}


// Countdown kalender

var countDownDate = new Date(2021, 2, 18, 0, 0, 0, 0);
var currentDate = new Date();

function getDayCount () {
  let timeDifference = countDownDate.getTime() - currentDate.getTime();
  let daysToGo = Math.floor(timeDifference / (1000*60*60*24));
  let strArr = daysToGo.toString().split("");
  if (daysToGo < 0) {
    document.getElementById("number1").style.display = "none";
    document.getElementById("number2").style.display = "none";
    document.getElementById("message").innerHTML = "Afgelopen!";
  } else if (daysToGo >= 10){
    document.getElementById("number1").innerHTML = strArr[0];
    document.getElementById("number2").innerHTML = strArr[1];
  } else {
    document.getElementById("number1").innerHTML = "0";
    document.getElementById("number2").innerHTML = strArr[0];
  }
}


// Fade-elementen laden

function colorTransitions () {
  transitionBlueToWhite("blueToWhite1");
  transitionWhiteToBlue("whiteToBlue1");
  transitionBlueToWhite("blueToWhite2");
  transitionWhiteToBlue("whiteToBlue2");
}

function transitionBlueToWhite (id) {
  let canvas = document.getElementById(id);
  let drCon = canvas.getContext("2d");
  drCon.canvas.width  = window.innerWidth;
  let grd = drCon.createLinearGradient(0, 0, 0, 100);
  grd.addColorStop(0, "rgb(31,20,93)");
  grd.addColorStop(1, "rgba(31,20,93,0)");
  drCon.fillStyle = grd;
  drCon.fillRect(0, 0, window.innerWidth, 300);
}

function transitionWhiteToBlue (id) {
  let canvas = document.getElementById(id);
  let drCon = canvas.getContext("2d");
  drCon.canvas.width  = window.innerWidth;
  let grd = drCon.createLinearGradient(0, 0, 0, 100);
  grd.addColorStop(0, "rgba(31,20,93,0)");
  grd.addColorStop(1, "rgb(31,20,93)");
  drCon.fillStyle = grd;
  drCon.fillRect(0, 0, window.innerWidth, 300);
}


// Accordeonmenu
var accordeonLength = 11;

function scrollUp () {
  window.scrollBy(0, -103);
}

function toggleElement (id) {
  closeAccordeon ();
  showElement(id);
}

function closeAccordeon () {
  for (let i = 1 ; i <= accordeonLength ; i++) {
    if (document.getElementById("content" + i).style.display == "flex") {
      hideElement(i);
    }
  }
}

function hideElement (id) {
  document.getElementById("content" + id).style.display = "none";
  document.getElementById("headWhite" + id).style.display = "flex";
  if (id != 1) {
    document.getElementById("headWhite" + (id-1)).style.borderBottomWidth = "2px";
  }
}

function showElement (id) {
  console.log("clicked id: " + id);
  document.getElementById("content" + id).style.display = "flex";
  document.getElementById("headWhite" + id).style.display = "none";
  if (id != 1) {
    document.getElementById("headWhite" + (id-1)).style.borderBottomWidth = "6px";
  }
}

function linkToAccordeon(id) {
  toggleElement(id);
  setTimeout(() => {  scrollUp();; }, 5);
}
